package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        for (int i = 1; i < 6; i++) { // 1-5月
            list1.add(i);
        }

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        for (int i = 4; i < 10; i++) { // 4-10月
            list2.add(i);
        }

        deleteRepeatData(list1);
        deleteRepeatData(list2);

        intersect(list1, list2);
        Integer integer[] = union(list1, list2);
        for (int i = 0; i < integer.length; i++) {
            // System.out.println("union(list1, list2);== "+ integer[i]);
        }

    }

    /**
     * 数组的交集
     * 
     * @param list1
     * @param list2
     */
    private static void intersect(ArrayList<Integer> list1,
            ArrayList<Integer> list2) {
        int size1 = list1.size();
        int size2 = list2.size();
        int a = 0;
        int b = 0;

        if (size1 > size2) {
            for (int i = 0; i < size1; i++) {
                for (int k = 0; k < size2; k++) {
                    if (list1.get(i) == list2.get(k)) {
                        System.out.println("list1.get(" + i + ") == "
                                + list1.get(i));
                    }
                }
            }

        } else {
            for (int i = 0; i < size2; i++) {
                for (int k = 0; k < size1; k++) {
                    if (list2.get(i) == list1.get(k)) {
                        System.out.println("list2.get(" + i + ") == "
                                + list2.get(i));
                        
                    }
                }
            }
        }
    }

    /**
     * 去掉数组中重复的数据
     * 
     * @param numList
     */
    private static void deleteRepeatData(ArrayList<Integer> numList) {

        Iterator<Integer> iterator = numList.iterator();
        int temp = -1;
        if (iterator.hasNext()) {
            temp = iterator.next();
        }

        while (iterator.hasNext()) {
            int i = iterator.next();
            if (i == temp) {
                iterator.remove();
            } else {
                temp = i;
            }
        }
        System.out.println(numList);

    }

    /**
     * 数组的并集 求两个字符串数组的并集，利用set的元素唯一性
     * 
     * @param arrayList1
     * @param arrayList2
     * @return
     */
    public static Integer[] union(ArrayList<Integer> arrayList1,
            ArrayList<Integer> arrayList2) {

        Set<Integer> set = new HashSet<Integer>();
        for (Integer in : arrayList1) {
            set.add(in);
        }

        for (Integer in : arrayList2) {
            set.add(in);
        }
        Integer[] integers = new Integer[] {};
        set.toArray(integers);
        return set.toArray(integers);

    }

}

