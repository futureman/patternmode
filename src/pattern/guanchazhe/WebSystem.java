package pattern.guanchazhe;

/*
 *  
 */
public class WebSystem extends WeatherData
{
    
    public static void main(String[] args)
    {
        SubjectInter subject = new WebSystem();
        subject.register(new WebDisplay());
        subject.operateSelf();
    }
    
    public void operateSelf()
    {
        System.out.print("do start");
        notifyNews();
        
    }
    
}
