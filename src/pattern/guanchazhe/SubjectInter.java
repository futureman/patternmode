package pattern.guanchazhe;

/**
 * 主题
 * //TODO 添加类/接口功能描述
 * @author liq
 */
public interface SubjectInter
{
    public void register(DisplayInter instance);
    
    public void remove(DisplayInter instance);
    
    public void notifyNews();
    
    public void operateSelf();
}
