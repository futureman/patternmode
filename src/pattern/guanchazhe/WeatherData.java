package pattern.guanchazhe;

import java.util.ArrayList;
import java.util.List;

/**
 * 主题基本操作抽象类
 * //TODO 添加类/接口功能描述
 * @author liq
 */
public abstract class WeatherData implements SubjectInter
{
    
    private List<DisplayInter> displayInters = new ArrayList<DisplayInter>();
    
    public void register(DisplayInter instance)
    {
        if (!displayInters.contains(instance))
        {
            displayInters.add(instance);
        }
    }
    
    public void remove(DisplayInter instance)
    {
        if (displayInters.contains(instance))
        {
            displayInters.remove(instance);
        }
        
    }
    
    public void notifyNews()
    {
        for (DisplayInter obj : displayInters)
        {
            obj.display();
        }
    }
    
}
